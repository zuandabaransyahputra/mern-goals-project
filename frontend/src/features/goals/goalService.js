import axios from 'axios'

const API_URL = 'api/goals'

//Create new goal
const createGoal = async (goalData, token) => {
    const config = {
        headers: {
            Authorization: `Bearer ${token}` //Send token ke headers, token didapat dari slice
        }
    }
    const response = await axios.post(API_URL, goalData, config)
    return response.data
}

//get User goal
const getGoals = async (token) => {
    const config = {
        headers: {
            Authorization: `Bearer ${token}` //Send token ke headers, token didapat dari slice
        }
    }
    const response = await axios.get(API_URL, config)
    return response.data
}

//Delete goal
const deleteGoal = async (goalId, token) => {
    const config = {
        headers: {
            Authorization: `Bearer ${token}` //Send token ke headers, token didapat dari slice
        }
    }
    const response = await axios.delete(`${API_URL}/${goalId}`, config)
    return response.data
}

const goalService = {
    createGoal,
    getGoals,
    deleteGoal
}

export default goalService